import java.util.Scanner;

public class Basic12 {
   public static void main(String[] args) {

      Scanner in = new Scanner(System.in);
      int mul;
      int n = in.nextInt();
      int m = n;

      if (n == 0) {
	System.out.println("Factorial("+m+") = 1");
	return;
      }

      mul = n;
      for (n = n-1; n > 0; n--) {
      	mul = mul * n;
      }
      
      System.out.println("Factorial("+m+") = "+mul);

   }
}
