# Introduction
## Problem 1: Interval
### Statement
Write a program that, given two intervals, computes the interval corresponding to their intersection, or tells that it is empty.

### Input
Input consists of four integer numbers a1, b1, a2, b2 that represent the intervals [a1,b1] and [a2,b2]. Assume a1≤ b1 and a2≤ b2.

### Output
Print “[]” if their intersection is empty, or “[x,y]” if this is their non-empty intersection.

### Public test cases
#### Input
20 30   10 40
#### Output
[20,30]

#### Input
10 20   10 20
#### Output
[10,20]

#### Input
20 30   10 20
#### Output
[20,20]

#### Input
10 20   30 40
#### Output
[]

## Problem 2: Rounding
### Statement
Write a program that reads a real number x≥0 and prints ⌊ x ⌋ (the floor of x), ⌈ x ⌉ (the ceiling of x), and the rounding of x.

### Input
Input consists of a real number x ≥ 0.

### Output
Print the floor of x, the ceiling of x, and the integer number closer to x (⌈ x ⌉ if there is a tie).

### Public test cases
#### Input
4.5
#### Output
4 5 5

#### Input
0.999999
#### Output
0 1 1

#### Input
123345.33
#### Output
123345 123346 123345


# First loops
## Problem 1: First numbers
### Statement
Write a program that reads a number n, and prints all numbers between 0 and n.

### Input
Input consists of a natural number n.

### Output
Print in order all natural numbers between 0 and n.

### Public test cases
#### Input
5
#### Output
0
1
2
3
4
5


## Problem 2: Top to bottom 
### Statement
Write a program that reads two numbers x and y, and prints all numbers between x and y (or between y and x), in decreasing order.

### Input
Input consists of two integer numbers x and y.

### Output

Print all integer numbers between x and y (or between y and x), in decreasing order.

### Public test cases
#### Input
3 7
#### Output
7
6
5
4
3

#### Input
5 -3
#### Output
5
4
3
2
1
0
-1
-2
-3

#### Input
-7 -7
#### Output
-7

## Problem 3: Multiplication table
### Statement
Write a program that reads a number n and prints the “multiplication table” of n.

### Input
Input consists of a natural number n between 1 and 9.

### Output
Print the “multiplication table” of n.

### Public test cases
#### Input
2
#### Output
2 * 1 = 2

2 * 2 = 4

2 * 3 = 6

2 * 4 = 8

2 * 5 = 10

2 * 6 = 12

2 * 7 = 14

2 * 8 = 16

2 * 9 = 18

2 * 10 = 20


## Poblem 4: Sum of squares
### Statement
Write a program that reads a natural number n, and prints the result of the following sum:
    1^2 + 2^2 + … + (n−1)^2 + n^2

### Input
Input consists of a natural number n.

### Output
Print the sum of the first n squares. (The summation has a closed formula, so a loop would not be necessary to solve this exercise.)

### Public test cases
#### Input
3
#### Output
14


#### Input
4
#### Output
30


