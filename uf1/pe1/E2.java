import java.util.Scanner;

public class E2 {
   public static void main(String[] args) {

      Scanner in = new Scanner(System.in);
      int a = in.nextInt();
      int b = in.nextInt();
      int c = in.nextInt();
      int d = in.nextInt();

      if (a==b && b==c && c==d) { // x x x x
        System.out.println(a+" 4");
      } else if (a==b && b==c && c!=d) { // x x x o
        System.out.println(a+" 3");
      } else if (a!=b && b==c && c==d) { // o x x x
        System.out.println(b+" 3");
      } else if (a==b && b!=c && b==d) { // x x o x
        System.out.println(a+" 3");
      } else if (a==c && b!=c && c==d) { // x o x x
        System.out.println(a+" 3");
      } else if (a==b) {
        if (c!=d) { // x x i j
          System.out.println(a+" 2");
        } else if (c==d && a!=c) { // x x o o
          System.out.println(a+" 2");
          System.out.println(c+" 2");
        }
      } else if (b == c) {
        if (a != d) {
          System.out.println(b+" 2");
        } else if (a == d && a != b) {
          System.out.println(a+" 2");
          System.out.println(b+" 2");
        }
      } else if (a == c) {
        if (b != d) {
          System.out.println(a+" 2");
        } else if (b == d && a != b) {
          System.out.println(a+" 2");
          System.out.println(b+" 2");
        }
      } else if (a == d) {
        if (b != d) {
          System.out.println(a+" 2");
        } else if (b == c && a != b) {
          System.out.println(a+" 2");
          System.out.println(b+" 2");
        }
      }

   }
}
