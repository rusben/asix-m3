import java.util.Scanner;

public class E3 {
  public static void main(String[] args) {

     Scanner in = new Scanner(System.in);

     int a = in.nextInt();
     int b = in.nextInt();
     int c = in.nextInt();

     if(a+b == c){
       System.out.println("+");
     } else if(a - b == c){
       System.out.println("-");
     } else if(a * b == c){
       System.out.println("*");
     } else if(b != 0) {
       if(a / b == c) {
         System.out.println("/");
       } else if(a % b == c){
         System.out.println("%");
       } else {
         System.out.println("IMPOSIBLE");
       }
     } else System.out.println("IMPOSIBLE");
   }
 }
