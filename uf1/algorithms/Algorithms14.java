/*
Write a program to fill a vector of 10 integers and show the sum of the values that are in positions of the vector multiple of 3
*/

import java.util.Random;

public class Algorithms14 {
   public static void main(String[] args) {

      int i, x;
      int[] a = new int[10];

      // Get a random Number
      // https://study.com/academy/lesson/java-generate-random-number-between-1-100.html
      int max = 49;
      int min = 1;

      // create instance of Random class
      Random randomNum = new Random();

      for (i=0; i<10; i++) {
        a[i] = min + randomNum.nextInt(max);
      }

      // Print the array
      for (i=0; i<10; i++) {
        System.out.println(a[i]);
      }

    }
}
