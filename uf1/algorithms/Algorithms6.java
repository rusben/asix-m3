import java.util.Scanner;

public class Algorithms6 {
   public static void main(String[] args) {

      Scanner in = new Scanner(System.in);
      int x;
      int greater = 0;
      int lower = 0;

      for (int i=0; i<10; i++) { 
        do { // Read a mark until we get a valid mark
          x = in.nextInt();
        } while (x > 10 || x < 0);

        // When the while is finished we have a valid note in x
        if (x >= 7) greater++;
        else lower++;
      }

      System.out.println("Marks greater or equal to 7: "+greater);
      System.out.println("Marks lower than 7: "+lower);
   }
}
