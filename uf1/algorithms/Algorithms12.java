/*
Write a program to fill a vector of 10 integers. Then the user will be asked for an integer. The program will tell you how many values are larger, how many smaller and how many are equal to the value entered?
*/

import java.util.Scanner;

public class Algorithms12 {
   public static void main(String[] args) {

      Scanner in = new Scanner(System.in);
      int i, x;
      int[] a = new int[10];
      int lower, greater, equal;

      lower = greater = equal = 0;

      for (i=0; i<10; i++) {
        x = in.nextInt();
        // Asigno a la posición i del vector el elemento
        a[i] = x;
      }

      System.out.println("Insert the number to find: ");
      // Preguntamos otro número para buscarlo
      x = in.nextInt();

      for (i=0; i<10; i++) {
        if (a[i] > x) {
          greater++;
        } else if (a[i] < x) {
          lower++;
        } else {
          equal++;
        }
      }

      System.out.println("Numbers greater than "+x+": "+greater);
      System.out.println("Numbers lower than "+x+": "+lower);
      System.out.println("Numbers equal to "+x+": "+equal);
   }
}
