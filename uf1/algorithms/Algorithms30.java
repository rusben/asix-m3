import java.util.Scanner;

public class Algorithms30 {
   public static void main(String[] args) {

     Scanner in = new Scanner(System.in);
     int x, y, grupos;
     Boolean hayIguales = false;

     grupos = 0;

      x = in.nextInt();
      if (x == -1) {
        System.out.println(grupos);        
        System.exit(0);
      }

      do {
        y = in.nextInt();

        if (x == y) { // Los números son iguales
          hayIguales = true;
        } else { // He encontrado un cambio
            if (hayIguales) {
              grupos++;
              hayIguales = false;
            }
        }

        x = y;

      } while (y != -1);

      System.out.println(grupos);

    }
}
