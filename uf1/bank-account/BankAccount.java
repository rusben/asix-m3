import java.util.Scanner;

// Operations:
// p) pin
// t) transfer
// g) get
// b) balance
// m) menu
// e) exit
//
// pin: Ask for a pin. Max 3 attempts. Otherwise exit.
// transfer: Add money to the balance.
// get: Get money from the balance.
// balance: Show the balance.
// menu: Show the menu.
// exit: Exit.
//
// https://stackoverflow.com/questions/13942701/take-a-char-input-from-the-scanner

public class BankAccount {
   public static void main(String[] args) {

     // Variable declaration
      Scanner in = new Scanner(System.in);
      float MAX_BALANCE = 10000000.00f;
      float balance = 5000.00f;
      int PIN = 1234;
      int attempts = 0;
      int userPIN;
      char menuOption;
      float money;

      System.out.println("Please enter your PIN code:");
      // Your code here:
      userPIN = in.nextInt();

    // Enter the PIN code
    while (PIN != userPIN) {
      attempts = attempts + 1;

      if (attempts >= 3) {
        System.out.println("To many incorrect attempts.");
        System.exit(1);
      } // Finish the execution

      System.out.println("Incorrect PIN.");
      System.out.println("Please enter your PIN code:");
      userPIN = in.nextInt();
    }

    printMenu();
    menuOption = in.next(".").charAt(0);

    // Loop until e
    while (menuOption != 'e') {

      if (menuOption == 'b') {
        System.out.println("Your balance is: "+balance);
      } else if (menuOption == 't') {
        System.out.println("Please enter the money you want to transfer.");
        money = in.nextFloat();

        // Increase the balance with money if
        if ((money + balance) <= MAX_BALANCE) {
          balance = balance + money;
          System.out.println("Your balance is: "+balance);
        } else {
          System.out.println("Error. Transfer not allowed. MAX_BALANCE would be exceeded.");
        }

      } else if (menuOption == 'g') {
        System.out.println("Please enter the money you want to get.");
          money = in.nextFloat();

          // We have balance to get from
          if (money <= balance) {
            balance = balance - money;
            System.out.println("Your balance is: "+balance);
          } else {
            System.out.println("Error. Operation not performed. You don't have enough funds.");
          }

      }

      printMenu();
      menuOption = in.next(".").charAt(0);
    }
    // Exit
   }

   public static void printMenu() {
      System.out.println("MENU");
      System.out.println("--------------");
      System.out.println("t) transfer");
      System.out.println("g) get");
      System.out.println("b) balance");
      System.out.println("m) menu");
      System.out.println("e) exit");
   }
}
