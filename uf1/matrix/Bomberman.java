import java.io.*;
import java.util.*;

public class Bomberman {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */

        Scanner in = new Scanner(System.in);
        int x = in.nextInt();
        int y = in.nextInt();
        int[][] a = new int[x][y];
        int fila, columna;
        fila = columna = 0;

        // Read the a matrix
        for (int i=0; i<a.length; i++) {
            for (int j=0; j<a[0].length; j++) {
                a[i][j] = in.nextInt();
            }
        }

        // Matriz resultante
        char[][] b = new char[x][y];

        // Rellenar la matriz resultante con almohadillas
        for (int i=0; i<b.length; i++) {
            for (int j=0; j<b[0].length; j++) {
                b[i][j] = '#';
            }
        }

        int bomb = in.nextInt();

        // Search the bomb
        for (int i=0; i<a.length; i++) {
            for (int j=0; j<a[0].length; j++) {
                // Busco la bomba en la matriz inicial
                // Para guardar su posición (fila-columna)
                if (a[i][j] == bomb) {
                    fila = i;
                    columna = j;
                }
            }
        }

        // Recorro la fila 'fila' para ponerla toda en @
        // Lo que va variando cada iteración es la columna
        for (int j=0; j<b[0].length; j++) {
            b[fila][j] = '@';
        }

        // Recorro la columna 'columna' para ponerla toda en @
        // Lo que va variando cada iteración es la fila
        for (int i=0; i<b.length; i++) {
            b[i][columna] = '@';
        }

        // Printar la matriz resultante
        for (int i=0; i<b.length; i++) {
            for (int j=0; j<b[0].length; j++) {
                System.out.print(b[i][j]+"");
            }
            System.out.println();
        }


    }
  }
