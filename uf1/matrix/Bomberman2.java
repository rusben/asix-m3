import java.io.*;
import java.util.*;

public class Bomberman2 {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */

        Scanner in = new Scanner(System.in);
        int x = in.nextInt();
        int y = in.nextInt();
        int[][] a = new int[x][y];
        int fila, columna;
        fila = columna = 0;

        // Read the a matrix
        for (int i=0; i<a.length; i++) {
            for (int j=0; j<a[0].length; j++) {
                a[i][j] = in.nextInt();
            }
        }

        // Matriz resultante
        char[][] b = new char[x][y];

        // Rellenar la matriz resultante con almohadillas
        for (int i=0; i<b.length; i++) {
            for (int j=0; j<b[0].length; j++) {
                b[i][j] = '#';
            }
        }

        int bomb = in.nextInt();

        // Search the bomb
        for (int i=0; i<a.length; i++) {
            for (int j=0; j<a[0].length; j++) {
                // Busco la bomba en la matriz inicial
                // Para guardar su posición (fila-columna)
                if (a[i][j] == bomb) {
                    fila = i;
                    columna = j;
                }
            }
        }

	// Recorremos la matriz y si la fila o la columna coinciden con la 
	// fila y columna actual, entonces ponemos @ sino #
        for (int i=0; i<b.length; i++) {
            for (int j=0; j<b[0].length; j++) {
		if (i == fila || j == columna) {
                	b[i][j] = '@';
		} else {
                	b[i][j] = '#';
		}
            }
        }

        // Printar la matriz resultante
        for (int i=0; i<b.length; i++) {
            for (int j=0; j<b[0].length; j++) {
                System.out.print(b[i][j]+"");
            }
            System.out.println();
        }


    }
  }
