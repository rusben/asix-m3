/*
Read two matrix and sum both
*/

import java.util.Scanner;

public class MatrixMultiply {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int x = in.nextInt();
		int y = in.nextInt();
		int[][] a = new int[x][y];

		// Read the a matrix
		for (int i=0; i<a.length; i++) {
			for (int j=0; j<a[0].length; j++) {
				a[i][j] = in.nextInt();
			}
		}


		int w = in.nextInt();
		int z = in.nextInt();
		int[][] b = new int[w][z];

		// Read the b matrix
		for (int i=0; i<b.length; i++) {
			for (int j=0; j<b[0].length; j++) {
				b[i][j] = in.nextInt();
			}
		}

		// Matriu resultant
		int[][] c = new int[x][z];

		// Compatibles
		if (y == w) {

			for (int i=0; i< c.length; i++) {
			    for (int j=0; j< c[0].length; j++) {
						// Calculamos el valor de c[i][k]
						for (int k=0; k<b.length;k++) {
								c[i][k] += a[i][j]*b[j][k];
						}
			    }
			}

      } else { // No compatibles
				System.out.println("No Compatibles");
      }

      // Print the matrix
      for (int i=0; i<c.length; i++) {
        for (int j=0; j<c[0].length; j++) {
            System.out.print(c[i][j]+" ");
        }
				System.out.println();
      }

   }
}
