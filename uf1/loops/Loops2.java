import java.util.Scanner;

public class Loops2 {
   public static void main(String[] args) {

      Scanner in = new Scanner(System.in);
      int x = in.nextInt();
      int max = x;

      while (x >= 0) {
        if (x > max) {
          max = x; // We update the max value
        }
        x = in.nextInt();
      }

      System.out.println("The maximum is: "+max);
   }
}
