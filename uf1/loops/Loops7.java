import java.util.Scanner;

public class Loops7 {
   public static void main(String[] args) {

      Scanner in = new Scanner(System.in);
      int x;

      int sum = 0;
      int count = 0;

      do {
        x = in.nextInt();

        if (x >= 0 && x <= 10) {
          sum = sum + x; // sum += x;
          count++;
        } else {
          System.out.println("Not added: "+x);
        }

      } while (x != -1);

      System.out.println("Sum: "+sum);
      System.out.println("Count: "+count);
      System.out.println("Average: "+sum/count);

   }
}
