import java.util.Scanner;

public class Loops5 {
   public static void main(String[] args) {

      Scanner in = new Scanner(System.in);
      int x = in.nextInt();
      int mul = 1;

      // Si el primer número es negativo la multiplicación es 0 y acaba
      if (x < 0) mul = 0;

      while (x >= 0) {
        mul = mul * x;
        System.out.println("The mul is: "+mul);
        x = in.nextInt();
      }

      System.out.println("The mul is: "+mul);
   }
}
