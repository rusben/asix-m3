/*
Write a program that given an array with it price of 10 products and a second array with code of these 10 products show us what is the cheapest price and product code.*/

import java.util.Random;

public class Arrays1 {
   public static void main(String[] args) {

     int codes[] = {8,7,9,10,11,12,13,14,15,17};
     int prices[] = {3,7,2,1,4,8,9,10,12,15};

     int min = prices[0];
     int position = 0;

     for (int i=0; i < prices.length; i++) {
       if (prices[i] < min) {
         min = prices[i];
         position = i;
       }
     }

     System.out.println("The code of the minimum element is: "+codes[position]+" at the position: "+position+" with price $"+prices[position]+".");


  }
}
