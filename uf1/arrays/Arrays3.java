/*
Write a program that reads a line of 10 integers and write them in an array.
*/

import java.util.Scanner;

public class Arrays3 {
   public static void main(String[] args) {

     Scanner entrada = new Scanner(System.in);

     int i;
     int[] a = new int[10];

     for (i=0; i < 10; i++) {
       a[i] = entrada.nextInt();
     }

     for (i=0; i < 10; i++) {
       System.out.println("Position: "+i+" Value: "+a[i]);
     }

     // Imprimir sólo los elementos mayores que 3
     for (i=0; i < a.length; i++) {
       if (a[i] > 3) System.out.println(a[i]);
     }

    // Imprimir el vector en orden inverso
    for (i=a.length-1; i >=0 ; i--) {
      System.out.println("Position: "+i+" Value: "+a[i]);
    }

    int sum = 0;
    // Suma los elementos del vector
    for (i=a.length-1; i >=0 ; i--) {
      sum = sum + a[i];
    }
    System.out.println(sum);

    // Imprime los elementos impares
    for (i=0; i < a.length; i++) {
      if (a[i] % 2 != 0) System.out.println(a[i]);
    }

    // Imprime los elementos en las posiciones impares
    for (i=0; i < a.length; i++) {
      if (i % 2 != 0) System.out.println(a[i]);
    }

    System.out.println("Recorrer el vector con un while:");
    // Recorrer el vector con un while e imprimimos cada posición
    int j = 0;
    while (j < a.length) {
      System.out.println(a[j]);
      j++;
    }

    

  }
}
