import java.util.Scanner;

public class AR324 {
   public static void main(String[] args) {

      Scanner in = new Scanner(System.in);

      int n, i, j,  pivot, steps;

	

      do {
	// Number of molecules
      	n = in.nextInt();
	int[] molecules = new int[n];
	
	pivot = 0;

	for (i=0; i<n; i++) {
	  molecules[i] = in.nextInt();
	  if (molecules[i] == 1) pivot = i;
	}

	steps = 0;

	j = pivot;
	do {
	  j = molecules[j]-1;
	  steps++;
	} while(molecules[j]!=1);


	
      	System.out.println("The pivot is: "+pivot);
      	System.out.println("The steps are: "+steps);


      } while (n != 0);

   }
}
