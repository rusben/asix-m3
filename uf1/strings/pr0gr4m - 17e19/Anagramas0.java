import java.util.Scanner;

public class Anagramas0 {
   public static void main(String[] args) {

   Scanner in = new Scanner(System.in);

   /*
    Leer dos cadenas de texto y una vez leídas comprobar si son un anagrama.

    Retorna "SI" o "NO"

   */

   String line;
   char[] v;

   char[] w;
   boolean change = false;
   boolean b=true;

   while (in.hasNextLine()){

     line=in.nextLine();
     line=line.replace(" ","");
     line=line.toLowerCase();
     v=line.toCharArray();

     line=in.nextLine();
     line=line.replace(" ","");
     line=line.toLowerCase();
     w=line.toCharArray();

     if (v.length==w.length){

       for (int i=0;i<v.length; i++){
         change = false;
         for (int j=0;j<w.length; j++){
           if (v[i]==w[j]){
             w[j]=' ';
             change = true;
             break;
           }
         }

         if (!change) {
          b = false;
          break;
         }
      }



     System.out.println(b ? "SI" : "NO");
    } else System.out.println("NO");
   }
   in.close();
  }
}
