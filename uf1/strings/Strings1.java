
import java.util.Scanner;

public class Strings1 {
   public static void main(String[] args) {

   Scanner in = new Scanner(System.in);
   String line;
   char[] lineArray;

     while (in.hasNextLine()) {
       line = in.nextLine();

       // Convierto la linea de String a array de carácteres
       lineArray = line.toCharArray();

      System.out.println(line);

       for (int i =0; i< lineArray.length; i++) {
         System.out.print(lineArray[i]);
       }

       System.out.println();
       System.out.println(line.length());

     }

     in.close();

   }
}
