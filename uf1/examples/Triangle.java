import java.util.Scanner;

public class Triangle {
   public static void main(String[] args) {
      // Read a number n
      // Pre: n > 0
      Scanner in = new Scanner(System.in);
      int n = in.nextInt();
      int i = 1;
      int j = 1;

      if (n > 0) {

        while (i <= n) {
          for (j=1; j <= i; j=j+1) {
              System.out.print("*");
          }

          System.out.println();
          i = i+1;
        }
      }

   }
}
