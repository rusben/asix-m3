import java.io.*;
import java.util.*;

public class Bowling {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        
        Scanner in = new Scanner(System.in);
        
        int bolos = in.nextInt();
        
        int ordenados = 0;
        int fila = 1;
        
        while (ordenados < bolos) {
            ordenados = ordenados + fila;
            fila++;
        }
        
        if (ordenados == bolos) System.out.println("true");
        else  System.out.println("false");
        
        
        
    }
}
