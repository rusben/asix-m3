import java.io.*;

class TestFiles {

    public static void main(String[] args) throws IOException {

      File f = new File("test.txt");
      System.out.println(f.getAbsolutePath());
      System.out.println(f.exists());

      //System.out.println(f.getParent());

      System.out.println(f.getName());
      f.createNewFile();
      //File.listsFiles();
      f.getAbsolutePath();

      System.out.println(f.isFile());
      System.out.println(f.isDirectory());

      // longitud en bytes
      System.out.println(f.length());
      System.out.println(f.lastModified());

      //mkdir();
      //delete();

      // renameTo(File destí)

      // false --> truncate file
      // true --> append al final del fichero
      // ESCRIBIR
      BufferedWriter outputStream = new BufferedWriter(new FileWriter(f, false));
      outputStream.write("OOOOOO.OOOOOO\n");
      outputStream.write("AAAAAA.AAAAAA\n");
      outputStream.write("KKKKKK.KKKKKK\n");
      outputStream.write("EEEEEE.EEEEEE\n");
      outputStream.close();

      // LEER
      BufferedReader inputStream = new BufferedReader(new FileReader(f));
      String line;
      while((line = inputStream.readLine()) != null) {
            String[] values = line.split("\\.");
            // https://stackoverflow.com/questions/14833008/java-string-split-with-dot

            for (int i = 0; i < values.length; i++) {
              System.out.println(values[i]);
            }

            System.out.println(line);
      }

      inputStream.close();

      // // Truncate File
      // // Machaca el contenido
      // File file = new File("myData");
      // FileWriter fw = new FileWriter(file, false);
      // fw.flush();
      // System.out.println("File truncated");

    }

}
