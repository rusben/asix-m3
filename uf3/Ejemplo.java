import java.io.*;

class Ejemplo {

    Dato[] datos = new Dato[100];

    public static void main(String[] args) throws IOException {
        File f = new File("datos.data");

        Ejemplo e = new Ejemplo();

        e.cargarDatos(f);

        // Modificamos el vector datos
        e.changeData(4, 121, "HELLO");
        e.changeData(7, 345, "HI");
        e.changeData(9, 45, "BYE");


        e.guardarDatos(f);


    }

    // Cambia la posición pos del vector datos
    void changeData(int pos, int a, String b) {
      if (pos >= 0 && pos < datos.length) this.datos[pos] = new Dato(a, b);
    }

    // Borra todos los datos del vector datos
    void clearDatos() {
      for (int i = 0; i < datos.length; i++) {
        this.datos[i] = null;
      }
    }

    // carga los datos del fichero f
    void cargarDatos(File f) throws IOException {

      clearDatos();

      BufferedReader inputStream = new BufferedReader(new FileReader(f));
      String line;
      int i = 0;
      while((line = inputStream.readLine()) != null) {
            String[] values = line.split(":");

            // Creamos el Dato con los valores que hemos leído
            Dato d = new Dato(Integer.parseInt(values[0]), values[1]);
            this.datos[i] = d;
            i++;
      }

      inputStream.close();

    }

    // Guarda los datos en el fichero f
    void guardarDatos(File f) throws IOException {
      BufferedWriter outputStream = new BufferedWriter(new FileWriter(f, false));

      // Creamos el fichero con el Dato
      for (int i = 0; i < datos.length; i++) {
        if (this.datos[i] instanceof Dato) {
          // Atención el carácter  :   es el separador y salto de línea al final del archivo
          outputStream.write(datos[i].a+":"+datos[i].b+"\n");
        }
      }

      outputStream.close();
    }

}

class Dato {
    int a;
    String b;

    Dato(int a, String b) {
      this.a = a;
      this.b = b;
    }
}
