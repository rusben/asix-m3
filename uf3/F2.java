import java.io.*;


class F2 {

  public static void main(String[] args)  throws IOException {
    File f = new File("F2.in");
    BufferedReader inputStream = new BufferedReader(new FileReader(f));

    Fruta[] cesta = new Fruta[100];

    String line;
    int i = 0;
    while((line = inputStream.readLine()) != null) {
          String[] values = line.split(":");

          // Creamos la Fruta
          Fruta fru = new Fruta(values[0], values[1], Integer.parseInt(values[2]));
          cesta[i] = fru;

          // Comprobación
          System.out.println(cesta[i].nombre);
          i++;
    }

  }

}

class Fruta​ {
  String nombre;
  String color;
  int​ gramos;

  Fruta(String n, String c, int g) {
    this.nombre = n;
    this.color = c;
    this.gramos = g;
  }
}
