class Main {

    public static void main(String[] args) {

      ElPuigMedia ep = new ElPuigMedia();

      Serie s1 = new Serie("Jurasik Park", 2, "Comedia", "Stephen King");
      Serie s2 = new Serie("Jurasik Park 2", 2, "Comedia", "Stephen King");
      Serie s3 = new Serie("Jurasik Park 3", 2, "Comedia", "Stephen King");


      ep.anadirSerie(s1);
      ep.anadirSerie(s2);
      ep.anadirSerie(s3);


      ep.entregarSerie(2);

      System.out.println(ep);

    }
}

/*

Crea ahora una clase Main que realice lo siguiente:
Crea un objeto ElPuigMedia con el constructor por defecto y otro con el constructor con todos los atributos. (Aprovecha las pruebas que hiciste anteriormente).

Para cada uno de los objetos ElPuigMedia:
Entrega algunos Videojuegos y algunas Series con los métodos entregarSerie() y entregarVideojuego.
Cuenta cuántas Series y Videojuegos hay entregados e imprime sus valores. Después devuelve algunos con los métodos devolverVideojuego() y devolverSerie().
Vuelve a contar cuántas Series y Videojuegos hay entregados e imprime sus valores.
Por último, indica qué Videojuego tiene más horas estimadas y la Serie con más temporadas. Utiliza los métodos getVideoJuegoConMasHoras() y getSerieConMasTemporadas().
Muéstralos en pantalla con toda su información usando el método toString().

*/
