class Serie {

  private String titulo;
  private int nTemporadas;
  private boolean entregado;
  private String genero;
  private String creador;

  // Un constructor por defecto. (Sin parámetros)
  public Serie() {
    this.titulo = "";
    this.creador = "";
    this.entregado = false;
    this.nTemporadas = 3;
    this.genero = "";

  }


  //Un constructor con título y creador. El resto los valores por defecto.
  public Serie(String titulo, String creador) {
		this.titulo = titulo;
		this.creador = creador;
    this.entregado = false;
    this.nTemporadas = 3;
    this.genero = "";
	}


  //Un constructor con todos los atributos, excepto de entregado.
	public Serie(String titulo, int nTemporadas, String genero, String creador) {
		this.titulo = titulo;
		this.nTemporadas = nTemporadas;
		this.genero = genero;
		this.creador = creador;
    this.entregado = false;
	}

	/**
	* Returns value of titulo
	* @return
	*/
	public String getTitulo() {
		return titulo;
	}

	/**
	* Sets new value of titulo
	* @param
	*/
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	/**
	* Returns value of nTemporadas
	* @return
	*/
	public int getNTemporadas() {
		return nTemporadas;
	}

	/**
	* Sets new value of nTemporadas
	* @param
	*/
	public void setNTemporadas(int nTemporadas) {
		this.nTemporadas = nTemporadas;
	}

	/**
	* Returns value of genero
	* @return
	*/
	public String getGenero() {
		return genero;
	}

	/**
	* Sets new value of genero
	* @param
	*/
	public void setGenero(String genero) {
		this.genero = genero;
	}

	/**
	* Returns value of creador
	* @return
	*/
	public String getCreador() {
		return creador;
	}

	/**
	* Sets new value of creador
	* @param
	*/
	public void setCreador(String creador) {
		this.creador = creador;
	}

  public void entregar() {
    this.entregado = true;
  }

  public void devolver() {
    this.entregado = false;
  }

  public boolean isEntregado() {
    return this.entregado;
  }

	/**
	* Create string representation of Serie for printing
	* @return
	*/
	@Override
	public String toString() {
		//return "Serie [titulo=" + titulo + ", nTemporadas=" + nTemporadas + ", entregado=" + entregado + ", genero=" + genero + ", creador=" + creador + "]";


    return "Título:\n"+
    this.titulo+
    "\nGénero:\n"+
    this.genero+
    "\nTemporadas:\n"+
    this.nTemporadas+
    "\nCreador:\n"+
    this.creador+
    "\nEstado:\n"+
    (this.entregado ? "ENTREGADO" : "NO ENTREGADO")+"\n";
	}
}

/*


Crea una clase llamada Serie con las siguientes características: (2 puntos)

Sus atributos son título, número de temporadas, entregado, género y creador.

Por defecto, el número de temporadas es de 3 temporadas y entregado false. El resto de atributos serán valores por defecto según el tipo del atributo.

Los constructores que se implementaran serán:
Un constructor por defecto. (Sin parámetros)
Un constructor con título y creador. El resto los valores por defecto.
Un constructor con todos los atributos, excepto de entregado.

Los métodos que se implementara serán:
Métodos get de todos los atributos, excepto de entregado.
Métodos set de todos los atributos, excepto de entregado.
Sobrescribe el método toString con el siguiente formato:


Título:
<VALOR_ATRIBUTO_TITULO>
Género:
<VALOR_ATRIBUTO_GENERO>
Temporadas:
<VALOR_ATRIBUTO_TEMPORADAS>
Creador:
<VALOR_ATRIBUTO_CREADOR>
Estado:
ENTREGADO o NO ENTREGADO (En función del valor del atributo)

AB) Para la clase Serie y la clase Videojuego:
    entregar(): cambia el atributo entregado a true.
    devolver(): cambia el atributo entregado a false.
    isEntregado(): devuelve el estado del atributo entregado.

*/
