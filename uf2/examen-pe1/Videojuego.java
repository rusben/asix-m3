class Videojuego {

  private String titulo;
  private int horasEstimadas;
  private boolean entregado;
  private String genero;
  private String compania;


  // Un constructor por defecto.
	public Videojuego() {
    this.titulo = "";
    this.horasEstimadas = 10;
    this.entregado = false;
    this.genero = "";
    this.compania = "";

	}

  // Un constructor con el título y horas estimadas. El resto por defecto.
  public Videojuego(String titulo, int horasEstimadas) {
    this.titulo = titulo;
    this.horasEstimadas = horasEstimadas;
    this.entregado = false;
    this.genero = "";
    this.compania = "";
  }


  // Un constructor con todos los atributos, excepto de entregado.
	public Videojuego(String titulo, int horasEstimadas, String genero, String compania) {
		this.titulo = titulo;
		this.horasEstimadas = horasEstimadas;
		this.entregado = false;
		this.genero = genero;
		this.compania = compania;
	}

	/**
	* Returns value of titulo
	* @return
	*/
	public String getTitulo() {
		return titulo;
	}

	/**
	* Sets new value of titulo
	* @param
	*/
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	/**
	* Returns value of horasEstimadas
	* @return
	*/
	public int getHorasEstimadas() {
		return horasEstimadas;
	}

	/**
	* Sets new value of horasEstimadas
	* @param
	*/
	public void setHorasEstimadas(int horasEstimadas) {
		this.horasEstimadas = horasEstimadas;
	}

	/**
	* Returns value of genero
	* @return
	*/
	public String getGenero() {
		return genero;
	}

	/**
	* Sets new value of genero
	* @param
	*/
	public void setGenero(String genero) {
		this.genero = genero;
	}

	/**
	* Returns value of compania
	* @return
	*/
	public String getCompania() {
		return compania;
	}

	/**
	* Sets new value of compania
	* @param
	*/
	public void setCompania(String compania) {
		this.compania = compania;
	}

  public void entregar() {
    this.entregado = true;
  }

  public void devolver() {
    this.entregado = false;
  }

  public boolean isEntregado() {
    return this.entregado;
  }

	/**
	* Create string representation of Videojuego for printing
	* @return
	*/
	@Override
	public String toString() {
		// return "Videojuego [titulo=" + titulo + ", horasEstimadas=" + horasEstimadas + ", entregado=" + entregado + ", genero=" + genero + ", compania=" + compania + "]";

    return "Título:\n"+
    this.titulo+
    "\nGénero:\n"+
    this.genero+
    "\nCompañía:\n"+
    this.compania+
    "\nHoras estimadas:\n"+
    this.horasEstimadas+
    "\nEstado:\n"+
    (this.entregado ? "ENTREGADO" : "NO ENTREGADO")+"\n";
	}
}


/*


Crea una clase Videojuego con las siguientes características: (2 puntos)
Sus atributos son título, horas estimadas, entregado, género y compañía.

Por defecto, las horas estimadas serán 10 horas y entregado false. El resto de atributos serán valores por defecto según el tipo del atributo.

Los constructores que se implementaran serán:
Un constructor por defecto.
Un constructor con el título y horas estimadas. El resto por defecto.
Un constructor con todos los atributos, excepto de entregado.

Los métodos que se implementarán serán:
Métodos get de todos los atributos, excepto de entregado.
Métodos set de todos los atributos, excepto de entregado.
Sobrescribe el método toString con el siguiente formato:

Título:
<VALOR_ATRIBUTO_TITULO>
Género:
<VALOR_ATRIBUTO_GENERO>
Compañía:
<VALOR_ATRIBUTO_COMPAÑIA>
Horas estimadas:
<VALOR_ATRIBUTO_HORAS_ESTIMADAS>
Estado:
ENTREGADO o NO ENTREGADO (En función del valor del atributo)

AB) Para la clase Serie y la clase Videojuego:
    entregar(): cambia el atributo entregado a true.
    devolver(): cambia el atributo entregado a false.
    isEntregado(): devuelve el estado del atributo entregado.

*/
