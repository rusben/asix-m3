
class Libro {
  private char[] ISBN = new char[10];
  private String titulo;
  private String autor;
  private int nPaginas;

  public Libro(char[] ISBN, String titulo, String autor, int nPaginas) {
    this.ISBN = ISBN;
    this.titulo = titulo;
    this.autor = autor;
    this.nPaginas = nPaginas;
  }

  public char[] getISBN() {
    return this.ISBN;
  }

  public void setISBN(char[] ISBN) {
    this.ISBN = ISBN;
  }

  public String getTitulo() {
    return this.titulo;
  }

  public void setTitulo(String titulo) {
    this.titulo = titulo;
  }

  public String getAutor() {
    return this.autor;
  }

  public void setAutor(String autor) {
    this.autor = autor;
  }

  public int getNPaginas() {
    return this.nPaginas;
  }

  public void setNPaginas(int nPaginas) {
    this.nPaginas = nPaginas;
  }

	@Override
	public String toString() {
    º
    return "El libro "+this.titulo+" con "+sISBN+" creado por "+this.autor+" tiene "+this.nPaginas+" páginas";
	}


}
