
class Main {
    public static void main(String[] args) {

      // Creamos varios Libros
      //Libro(char[] ISBN, String titulo, String autor, int nPaginas)

      Libro l1 = new Libro("1234567890".toCharArray(), "Las apasionantes clases de programación", "Rusben y el alumnado", 2);

      System.out.println(l1);

      Libro l2 = new Libro("1122334455".toCharArray(), "Terroríficas clases de programación", "Alumnos enfurecidos", 3);

      System.out.println(l2);

      if (l1.getNPaginas() > l2.getNPaginas()) {
        System.out.println("El libro con más páginas es "+l1.getTitulo());
      } else if (l2.getNPaginas() > l1.getNPaginas()) {
        System.out.println("El libro con más páginas es "+l2.getTitulo());
      } else {
        System.out.println("Los libros l1 y l2 tienen el mismo número de páginas.");
      }

      Biblioteca b1 = new Biblioteca("El Puig", 12);

      b1.addLibro(l1);
      b1.addLibro(l2);

      Libro l3 = b1.buscarLibroPorISBN("1122334455".toCharArray());
      System.out.println(l3);

      System.out.println(b1);


      Biblioteca b2 = new Biblioteca("Les Vinyes", 222);
      b2.addLibro(l1);
      b2.addLibro(l2);

      System.out.println(b2);



    }
}
