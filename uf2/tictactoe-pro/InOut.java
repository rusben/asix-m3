import java.io.*;
import java.util.*;

class InOut {

  /**
   * Pregunta por una coordenada x e y al usuario y retorna su valor.
   *
   * @return  la coordenada Coordenate que el usuario ha elegido.
   *
   */
  public static Coordenate askCoordenate(int player) {

    System.out.println("TURNO PLAYER "+player);
    System.out.println("Introduce una coordenada: ");

    Scanner in = new Scanner(System.in);

    int valorX = in.nextInt();
    int valorY = in.nextInt();

    Coordenate myCoordenate = new Coordenate();

    myCoordenate.x = valorX;
    myCoordenate.y = valorY;

    return myCoordenate;

  }

  /**
   * Clear the terminal screen
   *
   * @return  void
   */
  public static void clear() {
    System.out.print("\033[H\033[2J");
  }

  /**
   * Imprime por la salida estándar el resultado de la partida.
   *
   * @return  void
   */
  public static void printResult(Board b, Judge judge) {
    int ganador = judge.winner(b);

    if (ganador != 0) System.out.println("El ganador es el PLAYER "+ganador);
    else System.out.println("La partida ha acabado en empate.");
  }

}
