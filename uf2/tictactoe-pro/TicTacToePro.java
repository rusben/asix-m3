import java.io.*;
import java.util.*;

public class TicTacToePro {

    public static void main(String[] args) {

      // Declaración del tablero
      Board board = new Board();

      // Declaración del judge
      Judge judge = new Judge();

      // Declaración del objeto InOut
      InOut inout = new InOut();

      // Inicializamos el tablero
      board.initializeBoard();
      // Printar el estado del tablero
      board.printBoard();

      // Bucle del juego
      // Mientras el tablero no esté lleno y no haya ganador
      while (!judge.fullBoard(board) && judge.winner(board) == 0) {

        // Pedir una coordenada al usuario
        Coordenate tirada = inout.askCoordenate(judge.getTurn());

        // Si la coordenada es válida, realizar la jugada
        //if (validPlay(tirada)) {
        if (judge.validPlay(tirada.x, tirada.y, board)) {
          board.play(judge.getTurn(), tirada.x, tirada.y);
          //play(turn, tirada);
          judge.changeTurn();
        }

          inout.clear();
          board.printBoard();

        // En caso contrario (la tirada no es válida) hay que volverle a
        // pedir una coordenada al mismo jugador.
        // Por tanto, no hacemos nada si el movimiento es incorrecto y
        // se le vuelve a pedir al mismo jugador.
       }

       // Acaba el bucle porque ha habido un ganador o empate
       inout.printResult(board, judge);
    }
}
