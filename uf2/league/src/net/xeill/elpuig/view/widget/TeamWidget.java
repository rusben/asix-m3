package net.xeill.elpuig.view.widget;

import net.xeill.elpuig.model.Team;

public class TeamWidget {
    public static void list(Team[] teams){
        System.out.println(String.format("%4s   %-16s", "ID", "NOM"));
        for (int i = 0; i < teams.length; i++) {
          if (teams[i] != null) {
            System.out.println(String.format("%4s   %-16s", teams[i].getId(), teams[i].getName()));
          }

          System.out.println(i);
        }
        System.out.println();
    }

    public static void select(Team team){
      if (team != null) {
        System.out.println(String.format("\033[34mEQUIP => ID: %s   NOM: %s\033[0m", team.getId(), team.getName()));
      }
    }
}
