package net.xeill.elpuig.view.widget;

import net.xeill.elpuig.model.Player;

public class PlayerWidget {
    public static void list(Player[] players){
        System.out.println(String.format("%4s   %-16s", "ID", "NOM"));
        for (int i = 0; i < players.length; i++) {
          if (players[i] != null) {
            System.out.println(String.format("%4s   %-16s", players[i].getId(), players[i].getName()));
          }
        }
        System.out.println();
    }

    public static void select(Player player){
      if (player != null) {
        System.out.println(String.format("\033[34mEQUIP => ID: %s   NOM: %s\033[0m", player.getId(), player.getName()));
      }
    }
}
