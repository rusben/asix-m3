package net.xeill.elpuig.model;

public class Team {

  private int id;
  private String name;
  private int fundationYear;

  public Team(int id, String name, int fundationYear){
    this.id = id;
    this.name = name;
    this.fundationYear = fundationYear;
  }

  public int getId() {
    return this.id;
  }

  public void setId(int value) {
    this.id = value;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String value) {
    this.name = value;
  }

  public int getFundationYear() {
    return this.fundationYear;
  }

  public void setFundationYear(int value) {
    this.fundationYear = value;
  }

  

}
