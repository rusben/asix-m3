import java.io.*;
import java.util.*;

public class Card {

  private int value;
  private boolean hidden;

  public Card(int value, boolean hidden) {
    this.value = value;
    this.hidden = hidden;
  }

  public int getValue(){
    return this.value;
  }

  public void setValue(int value){
    this.value = value;
  }

  public boolean getHidden(){
    return this.hidden;
  }

  public void setHidden(boolean hidden){
    this.hidden = hidden;
  }
}
