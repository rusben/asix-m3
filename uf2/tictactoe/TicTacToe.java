import java.io.*;
import java.util.*;

public class TicTacToe {

    /**
     * Definición de la matriz que representa el tablero del juego
     *
     * Valores de la matriz y su significado
     *
     * 0 - Casilla vacía
     * 1 - Jugador X
     * 2 - Jugador O
     */
    static int[][] board = new int[3][3];

    // Definición de la variable que representa el turno del jugador
    static int turn = 1;

    public static void main(String[] args) {

      // Inicializamos el tablero
      initializeBoard();
      // Printar el estado del tablero
      printBoard();

      // Bucle del juego
      // Mientras el tablero no esté lleno y no haya ganador
      while (!fullBoard() && winner() == 0) {

        // Pedir una coordenada al usuario
        Coordenate tirada = askCoordenate();

        // Si la coordenada es válida, realizar la jugada
        //if (validPlay(tirada)) {
        if (validPlay(tirada.x, tirada.y)) {
          play(turn, tirada.x, tirada.y);
          //play(turn, tirada);
          changeTurn();
        }

          clear();
          printBoard();

        // En caso contrario (la tirada no es válida) hay que volverle a
        // pedir una coordenada al mismo jugador.
        // Por tanto, dentro de este else no hacemos nada, queda vacío.
       }

       // Acaba el bucle porque ha habido un ganador o empate
       printResult();

    }


    /**
     * Inicializa el tablero board, colocando un 0 en cada una de sus posiciones
     *
     * @return void
     */
    public static void initializeBoard() {
      for (int i = 0; i < board.length; i++) {
        for (int j = 0; j < board[0].length; j++) {
          board[i][j] = 0;
        }
      }
    }

    /**
     * Imprime por la salida estándar el estado actual del tablero board.
     * 0 - Casilla vacía
     * 1 - Jugador X
     * 2 - Jugador O
     *
     * @return void
     */
    public static void printBoard() {
      for (int i = 0; i < board.length; i++) {
        for (int j = 0; j < board[0].length; j++) {

          if (board[i][j] == 0) System.out.print("  | ");
          if (board[i][j] == 1) System.out.print("X | ");
          if (board[i][j] == 2) System.out.print("O | ");


        }
          System.out.println();
          System.out.println("___________");

      }
    }

    /**
     * Indica si el tablero board está lleno o no. Que el tablero está lleno significa
     * que no exite ninguna casilla en la que haya un 0.
     *
     * @return  true si el tablero está lleno, false en caso contrario.
     */

    public static Boolean fullBoard() {
      for (int i = 0; i < board.length; i++) {
        for (int j = 0; j < board[0].length; j++) {
          if (board[i][j] == 0) return false;
        }
      }
      return true;
    }

    /**
     * Coloca en la casilla indicada por la coordanada x e y la pieza indicada
     * en función del player elegido. Si el player es 1, colocará un 1 en la
     * matriz board, si el player es 2 colocará un 2.
     *
     * @param player  El jugador que realiza el movimiento (1 o 2)
     * @param tirada La coordenada x, y del tablero en la que se realiza el
     *               movimiento.
     *
     * @return  true si la pieza pudo colocarse en la coordenda indicada,
     *          false en caso contrario.
     */
    public static Boolean play(int player, Coordenate tirada) {

      if (player == 1 || player == 2) {
        board[tirada.x-1][tirada.y-1] = player;
        return true;
      }
      return false;

    }

    /**
     * Coloca en la casilla indicada por la coordanada x e y la pieza indicada
     * en función del player elegido. Si el player es 1, colocará un 1 en la
     * matriz board, si el player es 2 colocará un 2.
     *
     * @param player  El jugador que realiza el movimiento (1 o 2)
     * @param x La fila x del tablero en la que se realiza el movimiento
     * @param y La columna y del tablero en la que se realiza el movimiento
     *
     * @return  true si la pieza pudo colocarse en la coordenda indicada,
     *          false en caso contrario.
     */
    public static Boolean play(int player, int x, int y) {

        if (player == 1 || player == 2) {
          board[x-1][y-1] = player;
          return true;
        }
        return false;
    }


    /**
     * Indica si la coordenda x, y es una coordenada en la que se puede realizar
     *  un movimiento
     *
     * @param x La fila x del tablero en la que se realiza el movimiento
     * @param y La columna y del tablero en la que se realiza el movimiento
     *
     * @return  true si la coordenada  x, y, está del límite del tablero board y
     *               tiene un 0 (está vacía)
     *          false en caso contrario.
     */
    public static Boolean validPlay(int x, int y) {

      if (x >= 1 && x <= 3 && y >= 1 && y <= 3) {
        // Si la coordenada es correcta (dentro del límite)
        // Comprobamos que haya un 0 en esa posición
        if (board[x-1][y-1] == 0) return true;
      }

      return false;
    }

    /**
     * Indica si la coordenda tirada.x, tirada.y es una coordenada en la que se
     * puede realizar un movimiento
     *
     * @param tirada La coordenada x, y del tablero en la que se realiza el
     *               movimiento.
     *
     * @return  true si la coordenada  x, y, está del límite del tablero board y
     *               tiene un 0 (está vacía)
     *          false en caso contrario.
     */
    public static Boolean validPlay(Coordenate tirada) {
      if (tirada.x >= 1 && tirada.x <= 3 && tirada.y >= 1 && tirada.y <= 3) {
        // Si la coordenada es correcta (dentro del límite)
        // Comprobamos que haya un 0 en esa posición
        if (board[tirada.x-1][tirada.y-1] == 0) return true;
      }

      return false;
    }

    /**
     * Cambia el turno del juego.
     * Si el turno actual es 1 (tira el jugador 1), el turno turn pasa a ser 2.
     * Si el turno actual es 2 (tira el jugador 2), el turno turn pasa a ser 1.
     *
     * @return  void
     */
     public static void changeTurn() {
       if (turn == 1) turn = 2;
       else if (turn == 2) turn = 1;
    }

    /**
     * Indica si existe ganador en la posición actual de board.
     *
     * @return  true si existe un ganador en el tablero board actual.
     *          false en caso contrario.
     */
     // public static Boolean winner() {
     //  return false;
     // }

    /**
     * Indica si existe ganador en la posición actual de board.
     *
     * @return  0 si no hay ganador
     *          1 si gana el jugador 1
     *          2 si gana el jugador 2
     */
    public static int winner() {

      // Filas
      if (board[0][0] == board[0][1] && board[0][1] == board[0][2] && board[0][0] != 0) return board[0][0];
      if (board[1][0] == board[1][1] && board[1][1] == board[1][2] && board[1][0] != 0) return board[1][0];
      if (board[2][0] == board[2][1] && board[2][1] == board[2][2] && board[2][0] != 0) return board[2][0];

      // Columnas
      if (board[0][0] == board[1][0] && board[1][0] == board[2][0] && board[0][0] != 0) return board[0][0];
      if (board[0][1] == board[1][1] && board[1][1] == board[2][1] && board[0][1] != 0) return board[0][1];
      if (board[0][2] == board[1][2] && board[1][2] == board[2][2] && board[0][2] != 0) return board[0][2];

      // Diagonales
      if (board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[0][0] != 0) return board[0][0];
      if (board[0][2] == board[1][1] && board[1][1] == board[2][0] && board[1][1] != 0) return board[1][1];

      return 0;
    }

    /**
     * Imprime por la salida estándar el resultado de la partida.
     *
     * @return  void
     */
    public static void printResult() {
      int ganador = winner();

      if (ganador != 0) System.out.println("El ganador es el PLAYER "+ganador);
      else System.out.println("La partida ha acabado en empate.");
    }

    /**
     * Clear the terminal screen
     *
     * @return  void
     */
    public static void clear() {
      System.out.print("\033[H\033[2J");
    }


    /**
     * Pregunta por una coordenada x e y al usuario y retorna su valor.
     *
     * @return  la coordenada Coordenate que el usuario ha elegido.
     *
     */
    public static Coordenate askCoordenate() {

      System.out.println("TURNO PLAYER "+turn);
      System.out.println("Introduce una coordenada: ");

      Scanner in = new Scanner(System.in);

      int valorX = in.nextInt();
      int valorY = in.nextInt();

      Coordenate myCoordenate = new Coordenate();

      myCoordenate.x = valorX;
      myCoordenate.y = valorY;

      return myCoordenate;

    }

}

/**
 * Clase Coordenate que simula una coordenada x, y en la matriz que
 * representa nuestro tablero board.
 */
class Coordenate {
  /**
  * Atributos de la clase Coordenate
  */
  int x;
  int y;

  /**
  * Constructor de la clase Coordenate, crea un objeto vacío
  */
  public Coordenate() { }

  /**
  * Constructor de la clase Coordenate, crea un objeto con los valores pasados
  * como parámetro.
  */
  public Coordenate(int x, int y) {
    this.x = x;
    this.y = y;
  }
}
