class Contacto {
  private String nombre;
  private String telefono;

  Contacto() { }

  Contacto(String nombre, String telefono) {
    this.nombre = nombre;
    this.telefono = telefono;
  }

  public String getNombre() {
    return this.nombre;
  }

  public String getTelefono() {
    return this.telefono;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public void setTelefono(String telefono) {
    this.telefono = telefono;
  }

  public String toString() {
    return this.nombre+": "+this.telefono;
  }

}
