class Main {

  public static void main(String[] args) {

    Agenda a = new Agenda();

    Contacto b = new Contacto("Marc", "76522544");
    Contacto c = new Contacto("Rose", "3334513");
    Contacto d = new Contacto("Peter", "535455");
    Contacto e = new Contacto("Mary", "4352345");
    Contacto f = new Contacto("John", "12345678");

    a.insertaContacto(b);
    a.insertaContacto(c);
    a.insertaContacto(b);
    a.insertaContacto(e);
    a.insertaContacto(f);

    a.imprimirAgenda();

    a.eliminarContacto(b);

    a.buscarContacto("Marc");

    a.imprimirAgenda();

    a.buscarContacto("Mary");

    System.out.println(a.agendaLlena());
    System.out.println(a.huecosLibres());


  }

}
