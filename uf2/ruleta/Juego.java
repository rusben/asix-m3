class Juego {

  private Jugador[] jugadores;
  private Revolver r;

  Juego(int numJugadores) {

    if (numJugadores > 6 || numJugadores < 1) numJugadores = 6;

    this.jugadores = new Jugador[numJugadores];
    this.r = new Revolver();

    initJugadores();
  }

  private void initJugadores() {
    for (int i= 0; i < this.jugadores.length; i++) {
      this.jugadores[i] = null;
    }
  }

  void addJugador(Jugador j) {
    for (int i= 0; i < this.jugadores.length; i++) {
      if (this.jugadores[i] == null) {
        this.jugadores[i] = j;
        return;
      }
    }
  }

  boolean finJuego() {
    for (int i= 0; i < this.jugadores.length; i++) {
      if (!this.jugadores[i].isAlive()) {
        return true;
      }
    }
    return false;
  }

  void ronda() {
    for (int i= 0; i < this.jugadores.length; i++) {
      if (this.jugadores[i].disparar(this.r)) {
          // Ha palmado
          System.out.println("El jugador "+this.jugadores[i].getId()+" se dispara, ha muerto en esa ronda.");
          return;
      } else {
        // Sigue vivo
        System.out.println("El jugador "+this.jugadores[i].getId()+" se dispara, no ha muerto en esa ronda.");
      }
    }
  }


}

/*

Juego:
  Atributos:
      Jugadores (conjunto de Jugadores)
      Revolver
  Funciones:
      finJuego(): cuando un jugador muere, devuelve true
      ronda(): cada jugador se apunta y se dispara, se informara del estado de la partida (El jugador se dispara, no ha muerto en esa ronda, etc.)


El número de jugadores será decidido por el usuario, pero debe ser entre 1 y 6. Si no está en este rango, por defecto será 6.

En cada turno uno de los jugadores, dispara el revólver, si este tiene la bala  el jugador muere y el juego termina.


*/
