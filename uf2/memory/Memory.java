import java.io.*;
import java.util.concurrent.TimeUnit;
import java.util.*;

public class Memory {

    /**
     * Definición de las matriz que representan el tablero del juego
     *
     * Valores de la matriz y su significado
     *
     * cards - Contiene las cartas del juego (número del 1 al 8 que aparecen
     *         dos veces cada uno)
     * hidden - Indica qué cartas están descubiertas y qué cartas están giradas
     *          true - Carta oculta
     *          false - Carta descubierta
     */

    static int[][] cards = {{1, 1, 2, 2}, {3, 3, 4, 4}, {5, 5, 6, 6}, {7, 7, 8, 8}};
    static boolean[][] hidden = new boolean[4][4];
    static Scanner in = new Scanner(System.in);
    static int tries = 10;

    public static void main(String[] args) {

      // Inicializamos el tablero
      initializeBoard();
      // Printar el tablero (independientemente del estado)
      showBoard();
      // Esperar 5 segundos y empezar
      wait(5);
      clear();
      printBoard();

      // Bucle del juego
      // Mientras el juego no haya acabado
      while (!endGame()) {

        // Pedir dos coordenadas a = {x,y} b = {x,y}
        //System.out.println("Primera carta (x,y): ");
        int ax = in.nextInt();
        int ay = in.nextInt();

        //System.out.println("Segunda carta (x,y): ");
        int bx = in.nextInt();
        int by = in.nextInt();

        // Si la coordenada es válida, realizar la jugada
        if (validPlay(ax-1, ay-1, bx-1, by-1)) {
          showAndLetItBeIfDifferent(ax-1, ay-1, bx-1, by-1);
        }

        wait(3);
        clear();
        printBoard();

       }

       // Acaba el bucle porque ha habido un ganador o empate
       printResult();

    }


    /**
     * Inicializa el tablero board, colocando un 0 en cada una de sus posiciones
     *
     * @return void
     */
    public static void initializeBoard() {

      int intercambios = 25;
      int max = cards[0].length;
      int min = 0;

     // create instance of Random class
     Random randomNum = new Random();

     for (int i=0; i < intercambios; i++) {

        int ax = min + randomNum.nextInt(max);
        int ay = min + randomNum.nextInt(max);

        int bx = min + randomNum.nextInt(max);
        int by = min + randomNum.nextInt(max);

        swap(ax, ay, bx, by);

      }

      // Ocultar todas las cartas
      for (int i = 0; i < hidden.length; i++) {
        for (int j = 0; j < hidden[0].length; j++) {
            hidden[i][j] = true;
        }
      }

    }

    /**
     * Intercambia los valores a = {x,y} b = {x,y} en la matriz cards
     *
     * @return void
     */
    public static void swap(int ax, int ay, int bx, int by) {

      int tmp = cards[ax][ay];
      cards[ax][ay] = cards[bx][by];
      cards[bx][by] = tmp;

    }

    /**
     * Imprime por la salida estándar el estado actual del tablero cards.
     * * - Carta girada
     * Número entre 1 y 8 - Carta descubierta
     *
     * @return void
     */
    public static void printBoard() {
      printLine();
      for (int i = 0; i < cards.length; i++) {
        for (int j = 0; j < cards[0].length; j++) {

          if (hidden[i][j]) System.out.print("* ");
          else System.out.print(cards[i][j]+" ");

        }
          System.out.println();
      }
      printLine();
    }

    /**
     * Imprime por la salida estándar el estado actual del tablero cards.
     * * - Carta girada
     * Número entre 1 y 8 - Carta descubierta
     *
     * @return void
     */
    public static void showBoard() {
      printLine();
      for (int i = 0; i < cards.length; i++) {
        for (int j = 0; j < cards[0].length; j++) {

          System.out.print(cards[i][j]+" ");

        }
          System.out.println();
      }
      printLine();
    }

    /**
     * Indica si el tablero hidden tiene todas las cartas mostradas (todas las
     * posiciones a false)
     *
     * @return  true si el tablero hidden tiene todas las posiciones a false
     *          false en caso contrario.
     */

    public static Boolean endGame() {

      for (int i = 0; i < hidden.length; i++) {
        for (int j = 0; j < hidden[0].length; j++) {
            if (hidden[i][j]) return false;
        }
      }

      return true;
    }

    /**
     * Indica si las coordendas a = {x,y} b = {x,y} son coordeandas correctas
     * dentro de la matriz cards
     *
     * @param ax Coordenada x de la primera card a
     * @param ay Coordenada y de la primera card a
     * @param bx Coordenada x de la primera card b
     * @param by Coordenada y de la primera card b
     *
     * @return  true si si las coordendas a = {x,y} b = {x,y} son coordeandas
     *               correctas estando dentro del límite de cards
     *          false en caso contrario.
     */
    public static Boolean validPlay(int ax,int ay, int bx, int by) {

      if (ax >= 0 && ax < cards.length && ay >= 0 && ay < cards.length &&
          bx >= 0 && bx < cards[0].length && by >= 0 && by < cards[0].length) {
        return true;
      }

      return false;
    }

    /**
     * Imprime por la salida estándar el resultado de la partida.
     *
     * @return  void
     */
    public static void printResult() {
      System.out.println("You lose!");
    }

    public static void printLine() {
      System.out.println("----------");
    }

    /**
     * Clear the terminal screen
     *
     * @return  void
     */
    public static void clear() {
      System.out.print("\033[H\033[2J");
    }

    /**
     * Wait n seconds
     *
     * @return  void
     */
    public static void wait(int n) {

      try {
        TimeUnit.SECONDS.sleep(n);
      } catch(InterruptedException ex) {
        // Do something
      }
    }

    /**
     * Cambia en la matriz hidden de las cartas referenciadas por la coordeandas
     * a = {x,y} y b = {x,y} a descubiertas (valor false)
     *
     * @return  void
     */
    public static void showAndLetItBeIfDifferent(int ax, int ay, int bx, int by) {

      boolean initialA = hidden[ax][ay];
      boolean initialB = hidden[bx][by];

      hidden[ax][ay] = false;
      hidden[bx][by] = false;

      printBoard();

      if (cards[ax][ay] != cards[bx][by]) {
        hidden[ax][ay] = initialA;
        hidden[bx][by] = initialB;
      }

    }


}
