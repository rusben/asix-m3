public class Cuenta {
  private String titular;
  private double cantidad;

	/**
	* Default Cuenta constructor
	*/
	public Cuenta(String titular, double cantidad) {
		this.titular = titular;
		this.cantidad = cantidad;
	}

  /**
  * Cuenta constructor
  */
  public Cuenta(String titular) {
    this.titular = titular;
    this.cantidad = 0;
  }

	/**
	* Returns value of titular
	* @return
	*/
	public String getTitular() {
		return titular;
	}

	/**
	* Sets new value of titular
	* @param
	*/
	public void setTitular(String titular) {
		this.titular = titular;
	}

	/**
	* Returns value of cantidad
	* @return
	*/
	public double getCantidad() {
		return cantidad;
	}

	/**
	* Sets new value of cantidad
	* @param
	*/
	public void setCantidad(double cantidad) {
		this.cantidad = cantidad;
	}

	/**
	* Create string representation of Cuenta for printing
	* @return
	*/
	@Override
	public String toString() {
		return "TITULAR: "+ this.titular + " CANTIDAD:" + this.cantidad;
	}

  /**
  * Ingresa la cantidad en la cuenta
  * @return
  */
  public void ingresar(double cantidad) {
    if (cantidad > 0) {
      this.cantidad = this.cantidad + cantidad;
    }
  }

  /**
  * Retira la cantidad en la cuenta
  * @return
  */
  public void retirar(double cantidad) {
    // Si la cantidad que vamos a retirar es mayor o igual que la cantidad
    // de la cuenta entonces la podemos retirar
    if (cantidad <= this.cantidad) {
      this.cantidad = this.cantidad - cantidad;
    } else {
      System.out.println("ERROR, imposible retirar esta cantidad. Tu saldo es: "+this.cantidad);
    }
  }
}
