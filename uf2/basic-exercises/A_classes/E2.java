/*
    Completa el método E2.main()
        Se debe crear un objeto de la clase LineSeparator
        Establece el tamaño de la linea adecuadamente antes de cada print()
 */

class LineSeparator {
   int size;

   // Constructor por defecto
   LineSeparator() { }

   // Constructor de la classe con el parámetro s (size)
   LineSeparator(int s) {
     size = s;
   }

   // Cambia el size a s
   void setSize(int s) {
     size = s;
   }

   // Imprime los guiones que indique size
   void print(){
       for (int i = 0; i < size; i++) {
           System.out.print("-");
       }
       System.out.println();
   }
}

public class E2 {

   public static void main(String[] args) {

       // Creamos un objeto lineSeparator vacío
       LineSeparator lineSeparator = new LineSeparator();
       // Le asignamos el size 20 con setSize
       lineSeparator.setSize(20);

       System.out.println("Debajo de esta linea debe aparecer una linea de 20 guiones");
       lineSeparator.print();

       // Redeclaramos el objeto lineSeparator con size 10, directamente con el
       // constructor asignamos 10 al atributo size
       lineSeparator = new LineSeparator(10);

       System.out.println("Y debajo de esta uno de 10 guiones");
       lineSeparator.print();

   }
}
