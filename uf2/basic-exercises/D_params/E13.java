
/*
    Implementa los siguientes métodos:
        Bicycle.speedUp()
            Recibe un número entero, que indica la cantidad de velocidad que hay que sumar a la velocidad de la bici

        Bicycle.applyBrakes()
            Recibe un número entero, que indica la cantidad de velocidad que hay que restar. Después de restar
            la cantidad, la velocidad NO PUEDE quedar negativa.
 */


//class Bicycle {
//    String cyclist;
//    int speed;
//
//    public String toString(){
//        return cyclist + ": " + speed + " km/h";
//    }
//}
//
//public class E13 {
//
//    public static void main(String[] args) {
//        Bicycle b1 = new Bicycle();
//        Bicycle b2 = new Bicycle();
//        Bicycle b3 = new Bicycle();
//        Bicycle b4 = new Bicycle();
//
//        b1.cyclist = "Anquetil";
//        b2.cyclist = "Merckx  ";
//        b3.cyclist = "Hinault ";
//        b4.cyclist = "Indurain";
//
//
//        b1.speedUp(10);
//        printBicycles(b1, b2, b3, b4);
//
//        b1.speedUp(10);
//        printBicycles(b1, b2, b3, b4);
//
//        b4.speedUp(30);
//        printBicycles(b1, b2, b3, b4);
//
//        b1.applyBrakes(40);
//        printBicycles(b1, b2, b3, b4);
//
//        b2.speedUp(40);
//        b3.speedUp(50);
//        b4.speedUp(10);
//        printBicycles(b1, b2, b3, b4);
//
//        b1.applyBrakes(50);
//        b2.applyBrakes(50);
//        b3.applyBrakes(50);
//        b4.applyBrakes(50);
//        printBicycles(b1, b2, b3, b4);
//
//    }
//
//    static void printBicycles(Bicycle a, Bicycle b, Bicycle c, Bicycle d){
//        System.out.println(a);
//        System.out.println(b);
//        System.out.println(c);
//        System.out.println(d);
//        System.out.println("----------------");
//    }
//}
