
//import java.util.Random;
//import java.awt.*;
//
//class Ball {
//    int x, y;
//    float radius;
//    Color color;
//}
//
//class CollisionChecker {
//    boolean collide(Ball ball1, Ball ball2){
//
//        return true;
//    }
//}
//
//public class E16 {
//
//    public static void main(String[] args) {
//        Random random = new Random();
//	    Ball[] balls = { new Ball(), new Ball(), new Ball() };
//
//        for (int i = 0; i < balls.length; i++) {
//            balls[i].x = random.nextInt(10);
//            balls[i].y = random.nextInt(10);
//            balls[i].radius = 10; //random.nextFloat()*1000;
//            balls[i].color = Color.BLUE;
//        }
//
//
//        for (int i = 0; i < balls.length; i++) {
//            Canvas.draw(balls[i]);
//        }
//    }
//}
//
//class Canvas extends javax.swing.JFrame {
//    static Graphics2D g;
//
//    public Canvas() {
//        setSize(200,200);
//        setVisible(true);
//        setDefaultCloseOperation(EXIT_ON_CLOSE);
//        java.awt.Canvas canvas = new java.awt.Canvas();
//        canvas.setPreferredSize(new Dimension(200,200));
//        canvas.setIgnoreRepaint(true);
//        add(canvas);
//        g = (Graphics2D)canvas.getGraphics();
//        pack();
//    }
//
//    public static synchronized void draw(Object o) {
//        if(g==null) {
//            new Canvas();
//            try { Thread.sleep(200); } catch (Exception e) {}
//        }
//        int x=0,y=0,r=0;
//        Color c = Color.BLUE;
//        try {
//            r = o.getClass().getDeclaredField("radius").getInt(o);
//            x = o.getClass().getDeclaredField("x").getInt(o);
//            y = o.getClass().getDeclaredField("y").getInt(o);
//            c = (Color) o.getClass().getDeclaredField("color").get(o);
//        } catch (Exception e) {}
//
//        g.setColor(c);
//        g.fillOval(x, y, r*2, r*2);
//
//    }
//}
