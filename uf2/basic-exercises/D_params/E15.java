
/*
    Crea los siguiente métodos:
        Alumno.asignarNota()
            Recibe el codigo de asignatura y la nota, y la alamacena en el array. Hay tener en cuenta que la nota
            debe estar entre 0 y 10, y el codigo de asignatura dentro de los limites del array.

        Alumno.notaMedia()
            Retorna la nota media de las notas almacenadas en el array. Las notas con valor -1 no deben contar para el
            calculo de la nota media.
 */


//class Alumno {
//    float[] notas = { -1, -1, -1, -1, -1};
//}
//
//public class E15 {
//
//    public static void main(String[] args) {
//	    Alumno alumno = new Alumno();
//
//	    alumno.asignarNota(0, 8);
//	    alumno.asignarNota(1, 8);
//	    alumno.asignarNota(2, 8);
//	    alumno.asignarNota(3, 16f);
//	    alumno.asignarNota(4, 8f);
//	    alumno.asignarNota(5, 5f);
//	    alumno.asignarNota(6, 0f);
//
//        System.out.println(alumno.notaMedia());
//    }
//}
