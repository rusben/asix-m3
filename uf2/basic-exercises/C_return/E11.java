/*

Completa el método URL.toString() para que retorne una URL combinando correctamente los cuatro elementos.

IMPORTANTE: deberás añadir el modificador 'public' antes del tipo de retorno en la declaración del método.

 */

//class URL {
//    String protocol;
//    String domain;
//    String path;
//    String query;
//}
//
//public class E11 {
//
//    public static void main(String[] args) {
//	    URL problem458 = new URL();
//
//	    problem458.protocol = "https";
//	    problem458.domain = "www.aceptaelreto.com";
//	    problem458.path = "/problem/statement.php";
//	    problem458.query = "id=458";
//
//	    // debe imprimir    https://www.aceptaelreto.com/problem/statement.php?id=458
//        System.out.println(problem458.toString());
//    }
//}
