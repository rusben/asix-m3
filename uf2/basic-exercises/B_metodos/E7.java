/*
    Implementa la clase Dado.

    Crea el objeto 'dado' en el método E7.main()
 */

import java.util.*;

class Dado {
  int valor;

  void tirar() {
    int max = 6;
    int min = 1;

    Random randomNum = new Random();
    valor = min + randomNum.nextInt(max);
  }

}

public class E7 {

   public static void main(String[] args) {

       Dado dado = new Dado();

       dado.tirar();

       System.out.println(dado.valor);

   }
}
