/*
Añade los campos y métodos que faltan en la clase BoletinNotas
 */

class BoletinNotas {

  float[] notas = new float[5];
  float notaMedia;

   void inicializar(){
       notas = new float[5];
   }

   void calcularNotaMedia() {
     float total = 0;
     for (int i=0; i < notas.length; i++) {
       total = total + notas[i];
     }

     notaMedia = total / notas.length;
   }

   void asignarNota(int modulo, float nota ) {
    notas[modulo] = nota;
   }
}

public class E8 {

   public static void main(String[] args) {
	    BoletinNotas boletinNotas = new BoletinNotas();

      //boletinNotas.notas[0] = 9;
      boletinNotas.asignarNota(0, 9);

      boletinNotas.asignarNota(1, 9);
       //boletinNotas.notas[1] = 9;

       boletinNotas.notas[2] = 9;
       boletinNotas.notas[3] = 10;
       boletinNotas.notas[4] = 8.5f;

       boletinNotas.calcularNotaMedia();

       System.out.println(boletinNotas.notaMedia);

   }
}
